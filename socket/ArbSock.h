#ifndef ARBSOCK_H_
#define ARBSOCK_H_

#include "../util.h"
#include "ClientSocket.h"
#include "SocketException.h"
#include <iostream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <iomanip>

#define CURRENCIES 100

using namespace std;

class ArbSock
{
public:
    //member vars: the actual socket, and strings for passing back and forth
    ClientSocket* client_socket;
    string send;
    string reply;
    string user;
    string pass;

    ArbSock(string address, int port, string user, string pass) //construct an ArbSock object
    {
        this->user = user;
        this->pass = pass;
        client_socket = new ClientSocket(address, port);
        usleep(100000);
        *client_socket >> reply;
		cout << reply << endl;
    }
    ~ArbSock()
    {
        delete client_socket;
    }

    //------------------------------------
    // getStatus
    //
    // Gets the you current assets in all
    // all currencies
    //
    // @parameters : string username
    // 	           : string password
    //
    // @returns
    //          : vec<double>* with assets
    //-------------------------------------
    vector<double>* getStatus()
    {
        string sendme = this->user + " " + this->pass + " " + "getStatus\n";
        *client_socket << sendme;
        usleep(100000);
        *client_socket >> reply;
        vector<string> status = split(reply);
        vector<double> * assets = new vector<double>(CURRENCIES);

        // fill vector
        for(int ii = 0; ii < CURRENCIES ; ii++)
        {
            assets->at(ii) = (atof(status[ii+5].c_str()));
        }
		assets->push_back(atof(status[107].c_str()));
        return assets;
    }

    string getAllRates()
    {
        string sendme = this->user + " " + this->pass + " " + "getAllRates\n";
        *client_socket << sendme;
        usleep(100000);
		string str1;
		string str2;
        *client_socket >> reply;
        
		usleep(100000);

		string reply2 = "HELLO";
		int nlCount = count(reply.begin(), reply.end(), '\n'); //count of newlines
		while(nlCount < 100)	
		{
			reply2.clear();
			*client_socket >> reply2;
			reply += reply2;
			usleep(100000);
			nlCount += count(reply2.begin(), reply2.end(), '\n');
		}
		dump(reply);
        return reply;//+str2;
    }
    double getOneRate(int curr1, int curr2)
    {
        string curr1_str = convertInt(curr1);
        string curr2_str = convertInt(curr2);
        string sendme = this->user + " " + this->pass + " getOneRate " + curr1_str + " " + curr2_str + "\n";
        *client_socket << sendme;
        usleep(100000);
        *client_socket >> reply;
        vector<string> resp = split(reply);
        double ret = atof(resp.back().c_str());
        return ret;
    }
    void getAllRates( double** W, int V)
    {
        string response = getAllRates();
        int index = 0;
        vector<string> resVec = split(response);
		cout << "Building Adjacency List with "<< resVec.size() << " Elements"  <<endl;
        for(int ii = 0; ii < V; ii++)
        {
            for(int jj = 0; jj < V; jj++)
            {
                double str2doub = atof(resVec[index++].c_str());
                W[ii][jj] = str2doub;
            }
        }
    }
    string saveMe()
    {
        string sendme = this->user + " " + this->pass + "saveMe\n";
        *client_socket << sendme;
        usleep(100000);
        *client_socket >> reply;
        return reply;
    }


    double exchange( int curr1, double c1amt, int curr2, double c2amt)
    {
        string curr1_str = convertInt(curr1);
        string curr2_str = convertInt(curr2);
        string c1amt_str = convertDouble(c1amt);
        string c2amt_str = convertDouble(c2amt);
        string sendme = this->user + " " + this->pass + " exchange " + curr1_str + " " + c1amt_str + " " + curr2_str + " " + c2amt_str +  "\n";
        *client_socket << sendme;
        usleep(100000);
        *client_socket >> reply;

		cout << "\n--INFO--\n" << reply << "\n\n" << endl;

        double purchase_amount = -1.0;
        string key= "PURCHASED: ";
        size_t found = reply.rfind(key);

        if ( found !=string::npos )
        {
            string last_term = reply.substr(found+key.length(),reply.length());
            purchase_amount  = atof( last_term.c_str()  );
        }

        return purchase_amount;
    }
    string DONE()
    {
        //terminate the current session
        string sendme = "DONE\n";
        *client_socket << sendme;
        usleep(100000);
        *client_socket >> reply;
        return reply;
    }
private:
    //helpers and such
    vector<string> split(string const &input)
    {
        vector<string> ret;
        string sub;
        istringstream iss(input);
        while(iss>>sub)
        {
            ret.push_back(sub);
        }
        return ret;
    }
    string convertInt(int num)
    {
        stringstream ss;
        ss << setprecision(15) << num;
        return ss.str();
    }
    string convertDouble(double num)
    {
        ostringstream ss;
        ss << setprecision(15) <<  num;
        return ss.str();
    }
};

#endif
