#include "ClientSocket.h"
#include "SocketException.h"
#include "ArbSock.h"
#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;

int main ( int argc, char* argv[] )
{
/*  try
    {
     ClientSocket client_socket ( "128.197.185.27", 8001 );
		 string send; string reply;
      
		 while (send!="done") {
			 try 	{
				 cout << "CLIENT: ";
				 getline(cin,send); // i.e. get the whole line (this is important, cin >> send stops at a whitespace
				 client_socket << send+"\n";
				 sleep(1);  // give a little time between send and receive (1 second is usually overkill, but it's safe)
				 client_socket >> reply;
				 cout << "SERVER:  " << reply << endl;
			 }
			 catch ( SocketException& ) { cout << "Exception!" << endl;}
		 
		 }
		}
	catch ( SocketException& e )
		{
			cout << "Exception was caught:" << e.description() << "\n";
		}
	
	return 0;*/

	cout << "Connecting ... " << endl;
	ArbSock et("128.197.185.27", 8001, "JeffreyCrowell", "1e2e0e571a486530");
	cout << "Connected" << endl;
	
	cout << et.getStatus()->at(0) << endl;
	
//	cout << et.exchange(0,10.0,0,10.01 ) << endl;
//	cout << et.exchange(0,10.0,0,10.01 ) << endl;
//	cout << et.exchange(0,10.0,0,10.01 ) << endl;
//	cout << et.exchange(0,10.0,0,10.01 ) << endl;
	et.DONE();

	return 0;

}
