#ifndef _ARBITRAGE_GRAPH_H
#define _ARBITRAGE_GRAPH_H

#include "util.h"
#include <list>
#include <limits>
#include <cstdlib>
#include <iostream>
#include <ctime>


using namespace std;

typedef double** weight;
typedef list<int>::iterator node;

// ------------------------------------
// print
// 
// given a start index of aloop 
// We print the vertex and edgeweights
// -----------------------------------
void print(int X, double* D, int* PI , double** W)
{

	// Ensure we have a starting index
	if ( X < 0 )
	{
		return ;
	}

    // Print Loop Vertices
    int current = X;
	double edge_weight = W[current][PI[current]];
	double cycle_weight = edge_weight;
	cout  << current << "  ---[" << edge_weight<<"]"<<ends;

    while ( PI[current] != X )
    { 
       	current = PI[current];
	   	edge_weight = W[current][PI[current]];
	   	cycle_weight += edge_weight;
		cout << "--->  " <<  current << "  ---["  << edge_weight << "]" << ends;	
    }
    cout << "--->  "<< X << endl;
	cout << "Cycle Weight : " << cycle_weight  << endl;
    return;

}



//-------------------------------------------
// Intialize Graph
//
// @ modifies:
//     - PI : Sets all to NULL
//     - D  : Sets all to INF
//     - M  : Sets all to false
//-------------------------------------------
void init( int S,  int V, double* D, int* PI , bool* M)
{
    for ( int ii=0; ii<V; ii++ )
    {
        D[ii] = numeric_limits<double>::max( );
        PI[ii]=NULL;
		M[ii] = false;
    }

    D[S] = 0.0;
}

//--------------------------------------
// Relax Function
//
// Given a Vertex, PI and D, & W weights
//
// Relaxes all the vertices to that graph
//
// @modifies:
//     - PI : Gets new Predecessor
//     - D  : Shortest Path Weight Array
//--------------------------------------
void relax( int uu, int vv , weight W , double* D, int* PI )
{
    // If we see a shorter path than current
    if ( D[vv] > (D[uu] + W[uu][vv] ))
    {
        // Replace current path with shorter
        D[vv] = D[uu] + W[uu][vv];
        PI[vv] = uu;
    }
}

//-----------------------------------------
// Bellman Ford Algorithm
//
// @modifies:
//     - PI : Predecessor Array
//     - D  : Shortest Path Weight Array
//     - M  : Marked Vertices Array
//
// @returns :
//     True :  if there is a negative cycle
//     False:  if no negative cycle
//------------------------------------------
int isCycle( int S, int V , weight W, double* D, int* PI, bool* M )
{
    // STAGE 0 : Initialize Source
	init( S,V,D,PI,M);

	
	// STAGE 1 : Bellman Ford

    // Repeat V-1
    for ( int ii = 0; ii<(V-1) ; ii++  )
    {
        // Relax every Edge
        for ( int uu = 0; uu< V ; uu++)
        {
            for ( int vv= 0; vv< V; vv++ )
            {
                relax( uu, vv, W, D , PI);
            }
        }
    }

    // STAGE 2 : Extract Neg. Weight Cycles


	srand(time(NULL));
	int  OFFSET = rand()% V;
    // For every edge
    for ( int ii= 0; ii<V  ; ii++   )
    {
        for ( int jj=V-1; jj>=0 ; jj--)
        {
			int vv = (jj+OFFSET) % V ;
			int uu = (ii+OFFSET) % V ;
            cout << "vv "<< vv << endl;
			cout << "uu "<< uu << endl << endl;
			// Attempt to relax
            if ( D[vv] > D[uu] + W[uu][vv] )
            {
				// Mark current position as seen
				int X = vv;
				M[ X ] = true;
				
				cout << "ENTER CYCLE : " << vv << endl; 

				// Run backward until we see a loop
				while (  M[ PI[X] ]  != true )
				{
					X = PI[ X ];
					M[ X ] = true;

					cout << "LEADS TO " << X << endl;
				}

				// Return the position in cycle
                return PI[X] ;
            }
        }
    }
	// If no neg weighted cycle
    return -1;
}



//-------------------------------------------
// Overhead
//
// Returns the overhead associate with 
// entering a Neg Weight Cycle from the source
// 
//-------------------------------------------
double overhead(int S, int X, weight W)
{
	return W[S][X] + W[X][S];
}

//-----------------------------------------------
// BackTracker
//
// Given a graph with neg weight cycle
// that includes a vertex X.
// The entire cycle is determined and best entry
// and exit points
//
// @returns:
//      gain : the gain related to a cycle
//-----------------------------------------------
pair<double,int>* backtrack( int S, int X, weight W, double* D, int* PI , list<int>* path  )
{
	// Initialize current current vertex
    int current = X;
	// Compute edge weight form current vertex to previous vertex on cycle
   	double edge_weight = W[current][PI[current]];
    
	// Set pointers to keep track of the path
	path->clear();
	path->push_back( current );
	
	// Keep track of gain with given cycle
	double gain = edge_weight;
    
	// Initialize optimal vertex as current vertex
	int optimal = X;
	node current_node = path->begin();
	node optimal_node = path->begin() ;
    
	while  ( PI[current] != X )
    {
		// Move to predecessors
        current = PI[current] ;
		path->push_back( current );
		current_node++;
        
        // Update Total Cycle Weight 
        edge_weight = W[current][PI[current]];
		gain += W[current][PI[current]] ;
        
		// Update optimal cycle entry node if we see a better
        if ( overhead(S,current,W) < overhead(S,optimal,W) )
		{
			optimal = current;
			optimal_node = current_node;
		}	
	}
	// Adjust list to have it start at cycle node
	path->splice( path->begin(), *path, optimal_node, path->end()  );
    
	if ( *(path->end()) != optimal )
	{
		path->push_back(optimal);
	}
	
	// Append to the front and back the source if its not all ready the source
//	if ( *(path->begin()) != S )
//	{
//		path->push_front( S ); 
//	}
	
	// Append to the back if it is not source
//	if ( *(path->end()) != S )
//	{
//		path->push_back( S ); 	
//	}
	
	// return the cycle weight and 
	return new pair<double,int>(gain,optimal);
}


#endif
