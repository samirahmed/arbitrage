
#ifndef ARB_PREPROCESS_H
#define ARB_PREPROCESS_H
#include <cmath>

void preProcess(double** dataMatrix, int V)
{
	for(int ii = 0; ii < V; ii++)
	{
		for(int jj = 0; jj < V; jj++)
		{
			dataMatrix[ii][jj] = (-1.0) * log10(dataMatrix[ii][jj]);
		}
	}
}

// Returns the 10^num
double reverseLog( double num)
{
	double neg = -1.0 *  num;
	return pow( 10.0 , neg );
}

// Calculates -log_10(num)
double preProcess(double num)
{
	return  (-1.0)* log10(num)  ;
}

#endif
