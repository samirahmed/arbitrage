#ifndef _ARBITRAGE_TRANSCATION_H
#define _ARBITRAGE_TRANSCATION_H

#include <iostream>
#include <vector>
#include <list>
#include <math.h>
#include <iomanip>

#include "preProcess.h"
#include "socket/ArbSock.h"
#include "graph.h"
#include "util.h"

#define FEE 0.001


//--------------------------------------------------
//
// profit
//
// Given parameters of a strategy determine profit
//
// @parameters
// X : initial investment
// Y : expected outcome
// N : num of cycles
// T : transaction fee
// G : cycle gain (estimate)
// L : cycle entry overhead
// E : earnings
// C : cycle size
//
// Y = X*(1-L)*((1+G)^N) - (N+2)*T
//
// @returns
//     double :  the profit earned on X
//--------------------------------------------------
double profit( double X, int N, double L, double G , int C )
{
    double gain = pow(G, N );
    double fee = (double) (N*C+2)* (double)FEE;
//    cout << "gain: " << gain << " fee: " << fee <<  endl;
    if ( L < 0 )
    {
        L = 1.0;
    }

    return (X*L*gain - fee);
}

//--------------------------------------------------
// optimize
//
// determines the number of cycles required to meet
// target earnings
//
//--------------------------------------------------
int optimize( double X, double T, double L, double G , int C )
{
    cout << "Gain "  << G<< endl;
	cout << "Loss "  << L<< endl;
	cout << "Loop Size" << C << endl;
	double target = (1.0+T)*X;
    int cycles = 10;
    double earnings = profit(X, cycles, L, G , C);

    while( (earnings < target)  && (cycles < 50) )
    {
        cycles += 1;
        earnings = profit( X, cycles ,  L , G  , C);
        cout << "." << ends;
    }
    cout << endl;
    return cycles;
}

class Strategy {

public:

    // MEMBERS

    int source;					// SOURCE VERTEX
    double gain;				// LOOP GAIN
    double amount;	// PRINCIPAL INVESTMENT
    double overhead;			// LOOP OVERHEAD - cost of loop entry and exit
    vector<int>* cycle;			// NEG WEIGHT EDGE CYCLE
    bool hasOverhead;			// Indicates if we don't start in cylce
    int cycle_length;			// Length of the NEG EDGE CYCLE
    double target;				// Percentage earnings 0.00-1.00
    int N;
    // CONSTRUCTOR

    Strategy ( )
    {
        ;
    }

    Strategy (int source, list<int>* path , double investment, double gain, double overhead, double target )
    {
        this->source = source;
        this->amount=  investment;
        this->gain = reverseLog(gain);
        this->overhead = reverseLog(overhead);
        this->target = target;
        this->cycle = new vector<int>( path->begin() , path->end() );
        this->cycle_length = cycle->size();

        // Determine if loop entry overhead exists
        if (this->cycle->at(0) == this->source )
        {
            this->hasOverhead = false;
        }
        else
        {
            this->hasOverhead = true;
        }

        // Determine the optimal amount of loops required
        this->N = optimize(this->amount ,this->target, this->overhead , this->gain, this->cycle_length);

    }

//--------------------------------------------------
//
// Y = X*(Loop Entry Cost)*(Loop Gain) - (Transaction Cost)
//
// Y = X*(Earnings)
//
//--------------------------------------------------
    double execute( ArbSock* server  )
    {
        // Expected return
        double net_original = server->getStatus( )->at(100);
        double principle = this->amount;
        double expectation = 0.0;
        double received = this->amount;

        cout << "Started with " << principle << endl;

        /// Move to into loop
        if ( this->hasOverhead )
        {
            expectation = received * server->getOneRate( this->source, this->cycle->at(0) );
            received = server->exchange(this->source, received , this->cycle->at(0) , expectation);
            cout << "\nFrom Source to " << this->cycle->at(0) << endl;
            cout << "Expected: " << expectation  << endl;
            cout << "Received: " << received << endl;

        }

        // Make n loops
        for ( int nn =0;  nn< this->N ; nn++)
        {
			cout << "\nCYCLE: " << nn+1 << " of " << this->N <<  "\n\n" <<endl;
            for( int ii =0; ii< (this->cycle->size()-1) ; ii++)
            {

                // If we are not USD
                // If we are not in the first CYCLE
                if ( nn>0  && (this->cycle->at(ii) != 0 ) )
                {
                    // Get All ASSETS at Start Currency
                    vector<double> * assets = server->getStatus();
                    double savings = assets->at( this->cycle->at(ii) );


                    // SELECT AS MUCH AS WE CAN
                    if ( savings  > 1000 )
                    {
                        received = 1000;
                    }
                    else
                    {
                        received = savings;
                    }
                }

                expectation = received * server->getOneRate( this->cycle->at(ii),  this->cycle->at(ii+1) );
                received =  server->exchange( this->cycle->at(ii), received , this->cycle->at(ii+1), expectation );
                cout << "\nFrom "<<this->cycle->at(ii) <<" to " << this->cycle->at(ii+1) << endl;
                cout << "Expected: " << expectation  << endl;
                cout << "Received: " << received << endl;
            }
        }

        // Go back to USD
        if( this->hasOverhead )
        {
			
            if( this->cycle->at(0) != 0 )
            {
                // Get All ASSETS at Start Currency
                vector<double> * assets = server->getStatus();
                double savings = assets->at( this->cycle->at(0) );


                // SELECT AS MUCH AS WE CAN
                if ( savings  > 1000 )
                {
                    received = 1000;
                }
                else
                {
                    received = savings;
                }
            }


            expectation =  received * server->getOneRate( this->cycle->at(0) , this->source );
            received = server->exchange( this->cycle->at(0) , received, this->source , expectation );
            cout << "\nFrom "<<this->cycle->at(0) <<" to 0" << endl;
            cout << "Expected: " << expectation  << endl;
            cout << "Received: " << received << endl;
        }

        double net_new = server->getStatus()->at(100);
        cout << "Original Networth:\t" << setprecision(19) << net_original << endl;
        cout << "New Networth:\t\t"<< setprecision(19) << net_new << endl;
        cout << "Net Result\t\t " << net_new - net_original << endl;

        return received - principle;

    }




};



//---------------------------------------------------
// trade
//a
// given a strategy, the trade carries out
// transactions with the server
//
// @parameters
//       server : socket/
//
// @returns
//       double : representing the earnings in USD
//
//----------------------------------------------------
//trade(  ArbSock * server, Strategy* plan )
//{
//	getStatus();

//}


#endif
