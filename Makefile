build:
	g++ main.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o build.exe

test-smart:
	g++ test/smart-fraud.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o smart.o
test-flood:
	g++ test/flood.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o flood.o

test-fraud:
	g++ test/fraud.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o fraud.o

test-exchange:
	g++ test/exchange.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o exchange.o

test-utils:
	g++ test/utils.cpp -w -o utils.o

test-rates:
	g++ test/rates.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o rates.o

test-arbitrage:
	g++ test/arbitrage.cpp socket/ClientSocket.cpp socket/Socket.cpp -w -o arbitrage.o

test-optimize: 
	g++ test/optimize-dump.cpp -w -o optimize.o

test-status:
	g++ test/status.cpp socket/ClientSocket.cpp socket/Socket.cpp  -w -o status.o

test-graph:
	g++ graph.h test/graph.cpp util.h -w -o graph.o

test-backtrack:
	g++  test/backtrack.cpp graph.h util.h -w -o backtrack.o

test-dump:
	g++ test/dump.cpp -w -o dump.o

clean:
	rm -f *.o *.exe
