#include "../util.h"
#include "../graph.h"
#include "../preProcess.h"
#include "../socket/ArbSock.h"
#include "../socket/ClientSocket.h"
#include "../socket/SocketException.h"
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

using namespace std;

int main( int argc, char** argv )
{
	
	string* user;
	string* pass;

	if (  argc < 2 )
	{
		cout << "usage: Please specify argument as 'jeff' or 'samir'"  << endl;
		return 0;
	}
	else{
		string* argument = new string ( argv[1]);
		if ( argument->compare("jeff") == 0)
		{
			user = new string("JeffreyCrowell")  ;
			pass = new string("1e2e0e571a486530");
		}
		else if (argument->compare("samir") == 0)
		{
			user = new string("SamirAhmed");
			pass = new string("4d8f737937e0fe69");
		}
		else
		{
			cout << "argument must be either 'jeff' or 'samir'" << endl;
			return 0;
		}
	}

	// Makes a get Status call 
	ArbSock* server= new ArbSock("128.197.185.27", 8001, user->c_str(), pass->c_str() );
	
	cout << "Opening Socket " << endl;
	vector<double>* assets  =  server->getStatus(  ) ;
	
	cout << "Printing Assets " << endl;

	int ROWS = 4;
	for ( int ii = 0; ii<100/ROWS ;ii++)
	{
		cout<< (ii*ROWS)+0<<"\t" << assets->at( (ii*ROWS) +0) <<"\t\t" <<  ends;
		cout<< (ii*ROWS)+1<<"\t" << assets->at( (ii*ROWS) +1) <<"\t\t" <<  ends;
		cout<< (ii*ROWS)+2<<"\t" << assets->at( (ii*ROWS) +2) <<"\t\t" <<  ends;
		cout<< (ii*ROWS)+3<<"\t" << assets->at( (ii*ROWS) +3) <<  endl;
	}

	cout << "NETWORTH = " << assets->at(100) << endl;


	delete assets;

	server->DONE();

	cout << "Close Server" << endl;	
	
	return 0;

}

