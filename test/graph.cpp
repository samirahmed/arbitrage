#include <iostream>
#include "../graph.h"
#include "../util.h"
using namespace std;

// --------------------------------
// TEST 1
// Negative Weighted Edge and Cycle
// --------------------------------
void test1(void)
{
    // Vertex count is 3
    int V = 3;
    int S = 0;

    // Convert to pointer
    double ** W = new double *[3];
    W[0] =  new double[3];
    W[1] =  new double[3];
    W[2] =  new double[3];

    // Create PI, D and M arrays
    int* PI = new int[3];
    double * D = new double[3];
	bool * M = new bool[3];
    //    A  B  C
    // A  0  6  8
    // B  8  0  2
    // C  3 -5.3  0

    W[0][0] = 0.0 ;
    W[0][1] =  6.0 ;
    W[0][2] = 8.0;
    W[1][0] = 8.0 ;
    W[1][1] =  0.0 ;
    W[1][2] = 2.0;
    W[2][0] = 3.0 ;
    W[2][1] = -5.3 ;
    W[2][2] = 0.0;

	print( W , V);

    // Run Cycle Check
	int X  =  isCycle( S, V,W,D,PI,M );
    if ( X >= 0 )
    {
        cout << "Cycle Found" << endl;
        cout << "Passed Test"  <<  endl;
    }
    else
    {
        cout << "No Cycle Found" << endl;
        cout << "Failed BellmanFord Based Cycle Checking" << endl;
    }

    print(X,D,PI,W);

}

// -------------------------------
// TEST 2
// Negative Weighted Edge, No Cycle
// --------------------------------

      void test2 ( void)
{
    // Vertex count is 3
    int S = 0;
    int V = 3;

    // Convert to pointer
    double ** W = new double *[3];
    W[0] =  new double[3];
    W[1] =  new double[3];
    W[2] =  new double[3];

    // Create PI, D and M arrays
    int* PI = new int[3];
    double * D = new double[3];
	bool * M = new bool[3];

    //    A  B  C
    // A  0  6  8
    // B  8  0  2
    // C  3 -1  0

    W[0][0] = 0.0 ;
    W[0][1] =  6.0 ;
    W[0][2] = 8.0;
    W[1][0] = 8.0 ;
    W[1][1] =  0.0 ;
    W[1][2] = 2.0;
    W[2][0] = 3.0 ;
    W[2][1] = -1.0 ;
    W[2][2] = 0.0;

    // Run Cycle Checks, EXPECT NO CYCLE
 	int X = isCycle(S, V,W,D,PI,M ) ;
	if ( X >=0 )
    {
        cout << "Cycle Found" << endl;
        cout << "Failed BellmanFord Based Cycle Checking" << endl;
    }
    else
    {
        cout << "No Cycle Found" << endl;
        cout << "Passed Test"  <<  endl;
    }

   print(X,D,PI,W) ;
}

int main(void)
{


    cout << "---------\nTest 1\n------------" << endl;
    test1();


    cout << "\n--------\nTest 2\n------------" << endl;
    test2();

    return 0;
}
