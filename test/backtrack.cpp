#include <list>
#include "../graph.h"
#include <iostream>
#include "../util.h"
#include <utility>

using namespace std;

void test6by6(void)
{
    // Vertex count is 3
    int V = 6;
    int S = 0;

    // Convert to pointer
    double ** W = new double *[V];
    W[0] =  new double[V];
    W[1] =  new double[V];
    W[2] =  new double[V];
    W[3] =  new double[V];
    W[4] =  new double[V];
    W[5] =  new double[V];

    // Create PI, D and M arrays
    int* PI = new int[V];
    double * D = new double[V];
	bool * M =  new bool[V];
    //    A  B  C  D  E  F
    // A  0  1  6  2  3  7
    // B  1  0  2 -1  1  2
    // C  6  2  0  4 -1  4
    // D  2 -1  4  0  1  2
    // E  3  1 -1  1  0 -5
    // F  7  2  4  2 -5  0

    W[0][0] = 0.0 ;
    W[1][0] = 1.0 ;
    W[2][0]  = 6.0;
    W[3][0] = 2.0 ;
    W[4][0] = 3.0 ;
    W[5][0]  = 7.0;

    W[0][1]  = 1.0 ;
    W[1][1]  = 0.0 ;
    W[2][1]  = 2.0;
    W[3][1]  = -1.0;
    W[4][1]  = 1.0;
    W[5][1]  = 2.0;

    W[0][2] = 6.0 ;
    W[1][2] = 2.0 ;
    W[2][2] = 0.0;
    W[3][2] = 4.0;
    W[4][2] = -1.0;
    W[5][2] = 4.0;

    W[0][3] = 2.0 ;
    W[1][3] = -1.0 ;
    W[2][3] = 4.0;
    W[3][3] = 0.0;
    W[4][3] = 1.0;
    W[5][3] = 2.0;

    W[0][4] = 3.0 ;
    W[1][4] = 1.0 ;
    W[2][4] = -1.0;
    W[3][4] = 1.0;
    W[4][4] = 0.0;
    W[5][4] = -5.0;

    W[0][5] = 7.0 ;
    W[1][5] = 2.0 ;
    W[2][5] = 4.0;
    W[3][5] = 2.0;
    W[4][5] = -5.0;
    W[5][5] = 0.0;

	// Print Matrix
	print(W,V);

    // Run Cycle Check
    int X  =  isCycle( S, V,W,D,PI,M);
    if ( X >= 0 )
    {
		// Print Success Confirmation
        cout << "Found Cycle at " << X <<  endl;
        cout << "Cycle Test Passed" << endl;
		cout << "Running Arbitrage Test\n    ----   " << endl;
		// Create track to test
		list<int>* path =  new list<int>();
		
		// calculate gain and overhead
		pair<double,int>* results = backtrack(S,X,W,D,PI, path);
		double gain  = 	results->first;
		int O = results->second;
		double loss = overhead(S,O,W);
		
		cout << "OPTIMAL ENTRY AND EXIT POSITION: " << O << endl;
		cout << "GAIN: " << gain << endl;
		cout << "OVERHEAD: " << loss << endl;
		print ( path);

		
    }
    else
    {

        cout << "Failed to find a cycle " << endl;
        cout << "Test Failed" << endl;
		
		print(M,V);
		print(PI,V);
		print(D,V);
    }

    print( X, D, PI, W );


}

int main(void)
{

// Detect negative edge cycle
    cout << "\n---Six by Six Test-----\n" << endl;
    test6by6();
	return 0;
}
