#include "../util.h"
#include "../graph.h"
#include "../preProcess.h"
#include "../socket/ArbSock.h"
#include "../socket/ClientSocket.h"
#include "../socket/SocketException.h"
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

using namespace std;

int main( int argc, char** argv )
{
	
	string* user;
	string* pass;

	if (  argc < 2 )
	{
		cout << "usage: Please specify argument as 'jeff' or 'samir'"  << endl;
		return 0;
	}
	else{
		string* argument = new string ( argv[1]);
		if ( argument->compare("jeff") == 0)
		{
			user = new string("JeffreyCrowell")  ;
			pass = new string("1e2e0e571a486530");
		}
		else if (argument->compare("samir") == 0)
		{
			user = new string("SamirAhmed");
			pass = new string("4d8f737937e0fe69");
		}
		else
		{
			cout << "argument must be either 'jeff' or 'samir'" << endl;
			return 0;
		}
	}

	double** W = new double*[100];
	int V = 100;
	init( W,100,100,0.0 );

	// Makes a get Status call 
	ArbSock* server= new ArbSock("128.197.185.27", 8001, user->c_str(), pass->c_str() );
	
	cout << "Opening Socket " << endl;
//	cout << "ASSETS in USD : "  << server->getStatus()->at(0) << endl;

	cout << "Print Rates" << endl;
	server->getAllRates(W,100) ;
	preProcess(W,V);
	print(W,V);
	//print(W,100);
	cout << "Loaded Rates " << endl;
	cout << server->DONE() << endl; 
	//delete server;
    
    
	// Make datastructures
	double* D = new double[V];
	int * PI =  new int[V];
	bool * M =  new bool[V];
    
	int S = 0;
    
	int X = isCycle( S, V, W, D, PI, M);
    
	if ( X >= 0 )
	{
		cout << "found cycle" << endl;
		cout << "ENTRY VERTEX = " << X << endl;
		cout << "\n-- CYCLE --" << endl;
		print( X, D, PI, W);
        
		cout << "\n---\nCalculating gain and overhead"<< endl;
		
		// Make path list
		list<int>* path =  new list<int>();
        
		pair<double,int>* results = backtrack( S,X,W,D,PI, path);
		
		double gain  = results->first;
		int optimal_entry_vertex = results->second;
		double loss = overhead( S, optimal_entry_vertex , W );
        
		cout << "OPTIMAL ENTRY POSITION: " << optimal_entry_vertex << endl;
		cout << "GAIN: " << gain <<endl;
		cout << "OVERHEAD: " << loss <<  endl;
		print(path);
	}
	else
	{
		cout << "no neg weight cycle" << endl;
	}
	
	return 0;

}

