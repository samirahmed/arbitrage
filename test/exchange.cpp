#include "../util.h"
#include "../graph.h"
#include "../preProcess.h"
#include "../socket/ArbSock.h"
#include "../socket/ClientSocket.h"
#include "../socket/SocketException.h"
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

using namespace std;

int main( int argc, char** argv )
{
	
	string* user;
	string* pass;

	if (  argc < 4 )
	{
		cout << "usage: Please specify argument as 'jeff' or 'samir' followed by 'currency1' and 'currency2'"  << endl;
		return 0;
	}
	else{
		string* argument = new string ( argv[1]);
		if ( argument->compare("jeff") == 0)
		{
			user = new string("JeffreyCrowell")  ;
			pass = new string("1e2e0e571a486530");
		}
		else if (argument->compare("samir") == 0)
		{
			user = new string("SamirAhmed");
			pass = new string("4d8f737937e0fe69");
		}
		else
		{
			cout << "argument must be either 'jeff' or 'samir'" << endl;
			return 0;
		}
	}

	// MAKE CURRENCIES
	int AA = atoi(argv[2]);
	int BB = atoi(argv[3]);

	// Makes a get Status call
	ArbSock* server= new ArbSock("128.197.185.27", 8001, user->c_str(), pass->c_str() );

	cout << "\nCURRENT ASSETS\n" << endl;
	vector<double>* assets  =  server->getStatus(  ) ;
	cout << AA << ": " << assets->at(AA)<< endl;
	cout << BB << ": " << assets->at(BB) << endl;
	
	// Get RATE
	cout << "RATE FROM 1 -> 2" << endl;
	double rate  = server->getOneRate(AA,BB);

	double investment  = 10.0;

	double expected = investment * rate;

	// EXCHANGE
	double received = server->exchange( AA, investment ,  BB, expected );

	cout << "\nSOURCE TO DESTINATION\n" << endl;
	cout << "Market Rate:\t "<< rate << endl;
	cout << "Expected:\t "<< expected << endl;
	cout << "Received:\t "<< received << endl;

	// GET RATE

	rate = server->getOneRate(AA,BB);
	investment = received;
	expected = rate* investment;
	received = server->exchange( AA, investment, BB, expected);

	cout << "\nBACK TO SOURCE\n" << endl;
	cout << "Market Rate:\t "<< rate << endl;
	cout << "Expected:\t "<< expected << endl;
	cout << "Received:\t "<< received << endl;

	// GET STATUS LOOK AT ASSESTS AGAIN
	cout << "\nRESULTS\n" << endl;
	vector<double> * results = server->getStatus();
	cout << AA << ": " << results->at(AA) << endl;
	cout << BB << ": " << results->at(BB) << endl;
	

	cout << "Close Server" << endl;	
	
	return 0;

}

