#include "../util.h"
#include "../graph.h"
#include "../preProcess.h"
#include "../socket/ArbSock.h"
#include "../socket/ClientSocket.h"
#include "../socket/SocketException.h"
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

using namespace std;

int main( int argc, char** argv )
{

    string* user;
    string* pass;

    if (  argc < 3 )
    {
        cout << "usage: Please specify argument as 'jeff' or 'samir' and a base investment like '10' "  << endl;
        return 0;
    }
    else {
        string* argument = new string ( argv[1]);
        if ( argument->compare("jeff") == 0)
        {
            user = new string("JeffreyCrowell")  ;
            pass = new string("1e2e0e571a486530");
        }
        else if (argument->compare("samir") == 0)
        {
            user = new string("SamirAhmed");
            pass = new string("4d8f737937e0fe69");
        }
        else
        {
            cout << "argument must be either 'jeff' or 'samir'" << endl;
            return 0;
        }
    }

    double** W = new double*[100];
    int V = 100;
    init( W,100,100,0.0 );


    // Makes a get Status call
    ArbSock* server= new ArbSock("128.197.185.27", 8001, user->c_str(), pass->c_str() );

    cout << "Opening Socket " << endl;
//	cout << "ASSETS in USD : "  << server->getStatus()->at(0) << endl;
    server->getAllRates( W, V);
    preProcess(W,V);
    double * gain = new double[V];
    int best = 0;
    double best_rate = 0.0;
    cout << "SERVER RATE" << server->getOneRate( 0 , 1 ) << endl;
    cout << "OUR RATE" << W[0][1] << endl;
    double UPPER_BOUND = 10.0;
    double LOWER_BOUND = 0.15;
    for ( int ii = 0; ii < V ; ii++)
    {
        gain[ii] = overhead( 0, ii , W) ;
        double val =  gain[ii];

        // Ensure that our currency is not trading between x10 difference
        // If we have a better rate, remember it
        if ( ( reverseLog(W[0][ii]) <UPPER_BOUND ) &&
                ( reverseLog(W[0][ii]) > LOWER_BOUND) )
        {
            if ( val < best_rate) 
            {
                best = ii;
                best_rate = val;
            }

            cout << val << " at "  <<ii << " Rate=" << reverseLog( W[0][ii] ) << " best=" << best<<endl;
        }
		else
		{
		
		//	cout << "At " << ii << " Rate is only " << reverseLog( W[0][ii]) << endl;
		}

    }

    if ( best == 0)
    {
        cout << "NO FEASIBLE LOOPS" << endl;
        cout << endl;
    }
    int keep_going;
    cout << "Exploit US to " << best << " Rate ?" << endl;
    cin >> keep_going ;

    if (keep_going < 0)
    {
        return 0;
    }


    cout << "Loaded Rates " << endl;

    double BASE = 1.0* (double)atoi(argv[2]);
    double FRAUD_SIZE = 0.00;
    double investment = BASE;

    // Get the original networth
    double net_original =  server->getStatus()->at(100);

    for ( int ii = 0 ; ii < 50 ; ii++)
    {

        // FROM USD TO DESTINATION
        // Get All ASSETS at Start Currency
        investment = BASE;
        double expectation = investment * server->getOneRate( 0 ,  best ) + FRAUD_SIZE ;
        double received =  server->exchange( 0, investment , best , expectation );

        // FROM DESTINATION TO USD
        // SELECT AS MUCH AS WE CAN
        vector<double> * assets = server->getStatus();
        double savings = assets->at(best);

        if ( savings  > 1000 )
        {
            investment = 1000;
        }
        else
        {
            investment = savings;
        }

        expectation = investment * server->getOneRate( best ,  0 ) - FRAUD_SIZE ;
        received =  server->exchange( best, investment , 0 , expectation );

        cout << "LOOP" << ii  <<  "50" << endl;
        cout << "TRADE OFFER " <<  expectation  << endl;
        cout << "TRANSFER PERCENT " << (received/expectation) * 100 << endl;
    }

    double net_new = server->getStatus()->at(100);
    cout << "USD: \t" << server->getStatus()->at(0) << endl;
    cout << "DEST: \t" << server->getStatus()->at(best) << endl;
    cout << "Original Networth:\t" << setprecision(19) << net_original << endl;
    cout << "New Networth:\t\t"<< setprecision(19) << net_new << endl;
    cout << "Net Result\t\t " << net_new - net_original << endl;

    server->DONE();

}

