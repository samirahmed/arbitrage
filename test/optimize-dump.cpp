#include <iostream>
#include "../util.h"
#include <utility>
#include "../graph.h"
#include <list>
#include <string>
#include <fstream>
#include "../preProcess.h"
#include "../strategy.h"
#include <cmath>
#include <cstdlib>
#include <vector>
#include <sstream>
using namespace std;


vector<string> split(string const &input)
{
    vector<string> ret;
    string sub;
    istringstream iss(input);
    while(iss>>sub)
    {
        ret.push_back(sub);
    }
    return ret;
}

int main(void)
{
    string dump;
    ifstream dumpfile("data/dump.txt");
    getline(dumpfile, dump);
    while(dumpfile.good())
    {
        dump += "\n";
        string line;
        getline(dumpfile, line);
    	dump += line;
	}

    dumpfile.close();
    int V = 100;
	double** W = new double*[100];
    for(int ii = 0; ii < 100; ii++)
    {
        W[ii] = new double[100];
    }
    for(int ii = 0; ii < 100; ii++)
    {
        for(int jj = 0; jj < 100; jj++)
        {
            W[ii][jj] = 0;
        }
    }
	vector<string> spl = split(dump);
	int index = 0;
	for(int ii = 0; ii < V; ii++)
	{
		for(int jj = 0; jj < V; jj++)
		{
			double str2doub = atof(spl[index++].c_str());
			W[ii][jj] = str2doub;
		}
	}
	preProcess(W,V);
	print(W,V);

	// Make datastructures
	double* D = new double[V];
	int * PI =  new int[V];
	bool * M =  new bool[V];
    
	int S = 0;
    
	int X = isCycle( S, V, W, D, PI, M);
    
	if ( X >= 0 )
	{
		cout << "found cycle" << endl;
		cout << "ENTRY VERTEX = " << X << endl;
		cout << "\n-- CYCLE --" << endl;
		print( X, D, PI, W);
        
		cout << "\n---\nCalculating gain and overhead"<< endl;
		
		// Make path list
		list<int>* path =  new list<int>();
        
		pair<double,int>* results = backtrack( S,X,W,D,PI, path);
		
		double gain  = results->first;
		int optimal_entry_vertex = results->second;
		double loss = overhead( S, optimal_entry_vertex , W );
        
		cout << "OPTIMAL ENTRY POSITION: " << optimal_entry_vertex << endl;
		cout << "GAIN: " << gain <<endl;
		cout << "OVERHEAD: " << loss <<  endl;
		print(path);
		
		Strategy * strat =  new Strategy( S , path , 10.0 , gain, loss, 0.05);
		cout << "CYCLES REQUIRED = " << strat->N << endl;
	}
	else
	{
		cout << "no neg weight cycle" << endl;
	}

	return 0;
}
