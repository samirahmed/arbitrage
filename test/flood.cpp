#include "../util.h"
#include "../graph.h"
#include "../preProcess.h"
#include "../socket/ArbSock.h"
#include "../socket/ClientSocket.h"
#include "../socket/SocketException.h"
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>

using namespace std;

int main( int argc, char** argv )
{

    string* user;
    string* pass;

    if (  argc < 4 )
    {
        cout << "usage: Please specify argument as 'jeff' or 'samir' and Start and End Currency "  << endl;
        return 0;
    }
    else {
        string* argument = new string ( argv[1]);
        if ( argument->compare("jeff") == 0)
        {
            user = new string("JeffreyCrowell")  ;
            pass = new string("1e2e0e571a486530");
        }
        else if (argument->compare("samir") == 0)
        {
            user = new string("SamirAhmed");
            pass = new string("4d8f737937e0fe69");
        }
        else
        {
            cout << "argument must be either 'jeff' or 'samir'" << endl;
            return 0;
        }
    }

    int FROM = atoi( argv[ 2] );
    int TO = atoi( argv[ 3] );

    // Makes a get Status call
    ArbSock* server= new ArbSock("128.197.185.27", 8001, user->c_str(), pass->c_str() );

    cout << "Opening Socket " << endl;

    cout << "Loaded Rates " << endl;

    double investment = 0.0;

    // Get the original networth
    double net_original =  server->getStatus()->at(100);

    for ( int ii = 0 ; ii < 100 ; ii++)
    {

        // Get All ASSETS at Start Currency
        vector<double> * assets = server->getStatus();
        double FROM_ASSETS = assets->at( FROM );
        double TO_ASSETS = assets->at( TO );
		delete assets;

        // SELECT AS MUCH AS WE CAN
        if ( FROM_ASSETS  > 1000 )
        {
            investment = 1000;
        }
        else
        {
            investment = FROM_ASSETS;
        }

		if ( FROM_ASSETS < 1 )
		{
			break;
		}

        double expectation = investment * server->getOneRate( FROM ,  TO )  ;
        double received =  server->exchange( FROM, investment , TO , expectation );

        cout << "FROM ASSETS " <<  FROM_ASSETS << endl;
        cout << "TO ASSETS" << TO_ASSETS << endl;
    
	}

    double net_new = server->getStatus()->at(100);
    cout << "Original Networth:\t" << setprecision(19) << net_original << endl;
    cout << "New Networth:\t\t"<< setprecision(19) << net_new << endl;
    cout << "Net Result\t\t " << net_new - net_original << endl;

    server->DONE();

}

