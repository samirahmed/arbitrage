#include "../util.h"
#include "../strategy.h"

#include <iostream>
#include <list>

using namespace std;

int main( int argc, char** argv )
{
	// Lets say we have the following parameters

	double X = 10;		// Investment amount
	double T = 0.5;		// Target Earnings
	double G = 1.00609;	// Cycle Gain
	double L = .994308;	// Cycle Entry Overhead
	int C = 10;			// Cycle length

	cout << "LOOP GAIN, OVERHEAD AND SIZE: " << G << ", "<< L << ", " << C << endl;
	cout << "TARGET EARNING PERCENT : "<< T*100.0 << endl;
	cout << "INVESTMENT: " << X << endl;

	cout << "Cycles Required: " << optimize( X, T, L, G, C ) << endl;

	/// Verify with google search: 10*(0.994308)*((1.00609)^x) -(x*10+2)(0.001) - 10(1+T)

	return 0;
}

