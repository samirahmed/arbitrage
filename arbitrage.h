#ifndef _ARBITRAGE_ARBITRAGE_H
#define _ARBITRAGE_ARBITRAGE_H

#include "graph.h"
#include "strategy.h"
#include "util.h"
#include "socket/ArbSock.h"

#include <utility>
#include <iostream>
#include <list>
#include <vector>
#include <string>

#define CURRENCY_COUNT 100

using namespace std;

typedef double ** weight;
typedef list<int>::iterator node;

void arbitrage( string* user, string* password,double  principal,double target )
{

	// Create a new Adjacency Matrix of size VxV
	int V = 100;
	weight W = new double*[V];
	init(W, V,V, 0.0);
	
	// Make datastructures
	double* D = new double[V];
	int * PI =  new int[V];	
	bool * M =  new bool[V];
	list<int>* path =  new list<int>();
	int S = 0;


	// Create a Server Connection, get all the rates
	cout << "Connecting as " << user->c_str()  <<", " << password->c_str() << endl;
	ArbSock* server = new ArbSock("128.197.185.27", 8001 , user->c_str(), password->c_str()) ;
	cout << "Connection Established" << endl;
	
	// Generate Adjacency Matrix
	server->getAllRates( W, V );
	preProcess(W,V);


	cout <<  "Processing All Rates" << endl;

	// Check for cycle
	int X = isCycle(S,V,W,D,PI,M);
	if ( X>=0 )
	{
		
		// Print Cycle information
		cout << "Cycle Found: Possible Entry Vertex " << X << endl;
		cout << "\n--- CYCLE \n----\n" << endl;
		print( X, D, PI, W);
		
		// Backtrack and get optimal entry position and gain
		pair<double, int>* results = backtrack( S, X, W, D, PI, path);
		double gain = results->first;
		int entry = results->second;
		double loss = overhead(S, entry, W);

		// Devise a strategy
		Strategy* strategy = new Strategy( S , path , principal , gain, loss, target);
		
		cout << "Arbitrage: \nInvestment: "<< principal 
		<< "\nTarget %: " << target*100 
		<< "\nLoops Required: " << strategy->N << endl;

		// Execute the strategy
		double net = strategy->execute( server );

		server->DONE();

		print( X, D, PI, W);	
	}
	else
	{
		cout << "No Cycle Found " << endl;
	}

	

}


#endif

