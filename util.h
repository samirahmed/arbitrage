#ifndef ARBITRAGE_UTIL_H
#define ARBITRAGE_UTIL_H

#include <iostream>
#include <list>
#include <vector>
#include <fstream>

using namespace std;
using namespace std;

void dump(string content )
{
	// Dump to file
	ofstream dump;
	dump.open("data/dump.txt");
	dump << content;
	dump.close();

}

template<class T>
void print(const T *Array, int count)
{
    for (int ii=  0; ii < count; ii++)
    {
        cout<<Array[ii]<<"\t" << ends;
    }
    cout << endl;
}

template<class T>
void print(  T** mat, int length)
{
    for ( int ii=0; ii <  length ; ii++)
    {
        for( int jj=0; jj < length ; jj++ )
        {
            cout << mat[ii][jj] << "\t" << ends;
        }
        cout << endl;
    }
}

void print( list<int>* ll )
{
	list<int>::iterator it; 
	for ( it = ll->begin(); it != ll->end() ; it++ )
	{
		cout << *it << " " << ends;
	}

	cout << endl;
}

void print( vector<double>* ll)
{
	vector<double>::iterator it; 
	for ( it = ll->begin(); it != ll->end() ; it++ )
	{
		cout << *it << " " << ends;
	}
	cout << endl;
}

// ------------------------------------
// create an empty Template Class T** 
// to 2D array of value tt
// ------------------------------------
template<class T>
void init( T** matrix , int row, int col , T tt )
{
	for(int ii =0; ii<row ; ii++)
	{
		matrix[ii] = new T[row];
	}

	for ( int ii =0; ii < col;ii++)
	{
		for ( int jj =0; jj < col;jj++)
		{
			matrix[ii][jj] = tt;
		}
	}
}

#endif
